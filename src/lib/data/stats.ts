import type { Stat } from "$lib/utils/types";

export default [ 
  {
    statement: '# of phones freed',
    value: 5
  }, 
  {
    statement: '# of Laptops liberated',
    value: 1
  }, 
] as Stat[];

