// Base values for meta tags
// So they can be added as suffixes on different pages
// Via <svelte:head>

export const siteBaseUrl = '';

export const keywords = [
	'Free',
	'Freedom',
	'Free Software',
	'Open Source',
	'Blog',
	'The Big O Community',
	'Free software Community'
];

export const description =
	"Community homepage, where we share Free software and care for our human rights and privacy.";

export const title = 'The Big O Community';

export const image = `${siteBaseUrl}/images/logo.svg`;
