import type { Feature } from "$lib/utils/types";

export default [ 
  {
    name: 'Free your Phone',
    description:
      'Do you really own your phone? Learn how you can be safe and secure by using Free and Open Source Software on your phone.',
    image: 'images/features/free.png',
  }, 
  {
    name: 'Liberate your Laptop',
    description:
      'Learn how to install GNU/Linux on your Laptop/PC and give Freedom to your devices.',
    image: 'images/features/gnu-love.jpg',
  }, 
  {
    name: 'Connecting with Computers',
    description:
      'Sharing the knowledge of using Computers to people who needs the most.',
    image: 'images/features/connection.png',
  }
] as Feature[];

