import type { Member, Connect } from "$lib/utils/types";

export default [ 
  {
    title: 'Atul Raj',
    description:
      "I have always been a curious person, always want to know what's inside. How things work. This curiosity has lead me to the journey of exploration of many things. One of which is technology, electronics and computer software. When I use any software, I think about how this piece of program is created. I was a user of [proprietary software] and got disappointed that I can't peek inside the program. Then along the way I found out about [Free software], for me it was just to see what's inside but as I learned more about it, It fascinated me, the philosophy behind it, the benefit of community, respect towards each other. I just fell in love. So, I thought everyone should know about this philosophy that's why I am a member of The Big O Community.",
    coverImage: 'images/team/atul.png',
    socials: [
        {
            platform: 'mastodon',
            link:"https://mas.to/@atulyaraaj",
        },
        {
          platform: 'pixelfed',
          link: "https://pixelfed.social/i/web/profile/604569868706967546"
        }
    ] as Connect[]
  }, 
  
] as Member[];

