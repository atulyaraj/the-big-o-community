import features from '$lib/data/features';
import { filteredPosts } from '$lib/data/blog-posts';
import stats from '$lib/data/stats';


export async function load() {
  const posts = filteredPosts.slice(0, 4);

  return {
    features,
    posts,
    stats
  };
}
