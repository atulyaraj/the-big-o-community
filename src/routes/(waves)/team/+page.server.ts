import  members from '$lib/data/team/members';

export async function load() {
  return {
    members
  };
}
